package in.forsk.helloworld.iiitk_saurabh;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import in.forsk.helloworld.R;

public class Screen2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen2);

        String intent_string = "";

        Intent myIntent = getIntent();

        Bundle extras = myIntent.getExtras();

        if (extras != null) {
            intent_string = myIntent.getStringExtra("KEY");
        }

        Log.e("Second Activity", intent_string);
    }

    public void okButtonClick(View v) {
        Log.e("Screen 2", "ok button Called");
        Intent result =  new Intent();
        result.putExtra("result_key","Authenticate");
        setResult(RESULT_OK,result);
        finish();
    }

    public void cancelButtonClick(View v) {
        Log.e("Screen 2", "cancel button Called");

        finish();
    }
}
