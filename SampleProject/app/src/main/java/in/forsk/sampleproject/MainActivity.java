package in.forsk.sampleproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.androidquery.AQuery;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    Button loadBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView) findViewById(R.id.imageView1);
        loadBtn = (Button) findViewById(R.id.button1);

        loadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                new ImageLoaderAsyncTask(MainActivity.this, "http://iiitkota.forsklabs.in/appLogo.png", imageView).execute();
                new AQuery(MainActivity.this).id(imageView).image("http://iiitkota.forsklabs.in/appLogo.png");
            }
        });
    }
}
